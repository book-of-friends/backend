const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const swaggerUi = require('swagger-ui-express');
const { set } = require('mongoose');
const session = require('express-session');
const logger = require('./utils/logger');
const config = require('./utils/config');
const docs = require('./docs');
const dotenv = require('dotenv');

require('mongoose').set('debug', true);

//importation des fonctions des autres fichiers
const user = require('./controllers/user.controller');
const book = require('./controllers/book.controller');
const album = require('./controllers/album.controller');
const movie = require('./controllers/movie.controller');
const commun = require('./controllers/commun.controller');
const reservation = require('./controllers/reservation.controller');
const cover = require('./controllers/cover.controller');

dotenv.config();

require('mongoose').set('debug', true);

const app = express();
const router = express.Router();

app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: true,
}));

set('strictQuery', false);

router.use(cors({ origin: '*' }));
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json({ limit: '10mb' }));
router.use(cookieParser('your-secret-key'));
router.use(express.json());

app.use(router);

app.use('/', swaggerUi.serve, swaggerUi.setup(docs));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
    );
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

const multer = require('multer');
const storage = new multer.memoryStorage();

const upload = multer({ storage: storage });

// Routes
router.post('/cover', upload.single('file'), cover.addCover);
router.get('/cover/:filename', cover.getCover)

router.post('/auth/token', user.loginUser);
router.post('/auth/register', user.registerUser);
router.get('/user', user.getUser);
router.put('/user/update-password', user.update_Password);
router.put('/user/update-email', user.updateEmail);
router.post('/user/reset-password', user.newPasswordReset);
router.post('/user/password-oublie', user.resetPassword);
router.delete('/user', user.deleteUser);
router.put('/user', user.updateProfile);

router.get('/books', book.findBooks);
router.get('/mangas', book.findMangas);
router.get('/romans', book.findRomans);
router.post('/book', book.addBook);
router.get('/book', book.findBook);
router.delete('/book', book.deleteBook);
router.put('/book', book.updateBook);

router.post('/album', album.addAlbum);
router.get('/albums', album.findAlbums);
router.get('/album', album.findAlbum);
router.delete('/album', album.deleteAlbum);
router.put('/album', album.updateAlbum);

router.post('/movie', movie.addMovie);
router.get('/movies', movie.findMovies);
router.get('/movie', movie.findMovie);
router.put('/movie', movie.updateMovie);
router.delete('/movie', movie.deleteMovie);

router.get('/products', commun.getAllData);
router.get('/similar', commun.getSimilarities);

router.post('/reservation', reservation.makeReservation);
router.delete('/reservation', reservation.cancelReservation);
router.get('/reservation', reservation.getReservation);
router.get('/reservations', reservation.findReservations);

router.post('/comment', commun.addComment);
router.get('/comment', commun.getAllComments);

app.listen(config.PORT, () => {
    logger.info(`Server running on port ${config.PORT}`);
});
