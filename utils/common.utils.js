exports.getMissingFields = (fields) => {
    return Object.entries(fields)
        .filter(([key, value]) => {
            if (typeof value === 'number') {
                return value <= 0;
            } else if (Array.isArray(value)) {
                return value.length === 0;
            } else {
                return !value;
            }
        })
        .map(([key]) => key);
}

exports.sendResponse = (res, status, success, message, additionalInfo = null) => {
    res.status(status).json({ success, message, additionalInfo });
};

exports.checkMissingFields = (req, res, fields) => {
    const missingFields = exports.getMissingFields(fields);

    if (missingFields.length > 0) {
        exports.sendResponse(
            res,
            400,
            false,
            "Please fill in all required fields.",
            `Make sure to provide the required information for the following fields: ${missingFields.join(", ")}`
        );
        return false;
    } else {
        return true;
    }
};
