// create the client class
const { MongoClient } = require('mongodb');
const config = require("../utils/config");
const mongodb = require("mongodb");

async function initializeMongoDB() {
    try {
        const client = new MongoClient(config.MONGODB_URI);

        await client.connect();
        const db = client.db("book-of-friends")
        const bucket = new mongodb.GridFSBucket(db);

        // Return the initialized MongoDB components for reuse if needed
        return { bucket, db };
    } catch (error) {
        console.error("Error connecting to MongoDB:", error);
        throw new Error("Failed to connect to MongoDB");
    }
}

// export initializeMongoDB
exports.initializeMongoDB = initializeMongoDB;