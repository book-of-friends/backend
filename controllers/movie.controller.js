const Movie = require('../models/movie.model');
const Reservation = require('../models/reservation.model');
const { error } = require('../utils/logger');

const { checkMissingFields, sendResponse} = require('../utils/common.utils');
const { authenticateUserForArticle } = require("../Middleware/auth.middleware");
const {ObjectId} = require("bson");


// Function that allows adding a movie or series to the database
exports.addMovie = async (req, res) => {
    try {
        // User authentication
        const isAuth = await authenticateUserForArticle(req, res);
        if (!isAuth) return;

        // Extract data from the request body
        const {
            title, synopsis, rating, coverURL, releaseDate, seasons, author,
            genres, country, duration, language, type, actors, movieState
        } = req.body;

        const isMissing = checkMissingFields(req, res, { title, synopsis, rating, coverURL, releaseDate, seasons, author,
            genres, country, duration, language, type, actors, movieState});

        if (!isMissing) return;

        // Create the object representing the movie/series to be added
        const id = new ObjectId();
        const movieToSend = {
            _id: id, title, synopsis, rating, coverURL, releaseDate, seasons, author,
            genres, country, duration, language, type, actors, movieState
        };

        // Add data to the database
        const response = await Movie.create(movieToSend);

        // Check if the addition was successful
        if (!response) {
            return sendResponse(res, 500, false, "An error occurred during addition");
        }

        // Successful addition to the database
        return sendResponse(res, 201, true, "The movie/series has been added to the list");
    } catch (error) {
        console.error("Error adding movie/series:", error);
        // An unexpected error occurred during the process
        return sendResponse(res, 500, false, "An error occurred during addition");
    }
};


// Function to retrieve all movies/series from the database
exports.findMovies = async (req, res) => {
    try {
        // Retrieve all movies/series from the database
        const movies = await Movie.find();

        if (movies && movies.length > 0) {
            // Transform the data for display
            const data = movies.map(movie => ({
                id: movie._id.toString(),
                title: movie.title,
                author: movie.author || "Unknown",
                coverURL: movie.coverURL,
                type: movie.type,
                genres: movie.genres.map(genre => genre.value),
                language: movie.language
            }));

            // Return the transformed data
            return res.status(200).json(data);
        } else {
            // No movies/series found
            return sendResponse(res, 404, false, "No movies/series found");
        }
    } catch (error) {
        // An error occurred during the retrieval process
        return sendResponse(res, 500, false, "An error occurred while retrieving movies/series");
    }
};


/*
    Function that retrieves a movie/series from the database based on the provided ID parameter.
    Depending on the document type, it retrieves the necessary information,
    and based on the season reservations, manipulates the data for display.
*/
exports.findMovie = async (req, res) => {
    const {Id} = req.query;

    try {
        if (!Id) {
            return sendResponse(res, 400, false, "The route requires a parameter");
        }

        const movie = await Movie.findById(Id);

        if (!movie) return sendResponse(res, 404, false, "No album found");

        const availability = await Reservation.find({ articleId: Id});
        const volumeAvailability = getVolumeAvailability(movie.seasons, availability);

        const dataToSend = formatDataToSend(movie, volumeAvailability);
        return res.status(200).json(dataToSend);
    } catch (err) {
        error("Error:", err);
        return sendResponse(res, 500, false, "An error occurred while retrieving the album");
    }
};

// Function to determine the availability of volumes based on reservations.
function getVolumeAvailability(seasons, availability) {
    const volumeAvailability = [];

    const seasonNumbers = [...Array(parseInt(seasons)).keys()].map(e => `s.${e + 1}`);
    seasonNumbers.forEach((seasonNumber) => {
        const reservation = availability.find((entry) => entry.articleNumber === seasonNumber);
        if (reservation) {
            if (reservation.state === "Borrowed") volumeAvailability.push({ volume: seasonNumber, availability: `${reservation.state} - Will be returned on ${reservation.dateReturn}` });
            else volumeAvailability.push({ volume: seasonNumber, availability: reservation.state });
        } else {
            volumeAvailability.push({ volume: seasonNumber, availability: "Available" });
        }
    });

    return volumeAvailability;
}

// Function to format data before sending it as a response.

function formatDataToSend(movie, volumeAvailability) {
    return {
        id: movie._id.toString(),
        title: movie.title,
        synopsis: movie.synopsis,
        rating: movie.rating,
        coverURL: movie.coverURL,
        releaseDate: movie.releaseDate,
        seasons: movie.seasons,
        author: movie.author,
        genres: movie.genres,
        country: movie.country,
        duration: movie.duration,
        language: movie.language,
        movieState: volumeAvailability,
        type: movie.type,
        actors: movie.actors,
    };
}


// Function to update a movie/series in the database using the provided ID as a parameter.
exports.updateMovie = async (req, res) => {
    try {
        // User authentication
        const isAuth = await authenticateUserForArticle(req, res);
        if (!isAuth) return;

        // Retrieve the ID from the request parameters
        const { Id } = req.query;

        // Check for the presence of the ID in the request parameters
        if (!Id) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });

        // Extract data from the request body
        const {
            title, synopsis, rating, coverURL, releaseDate, seasons, author,
            genres, country, duration, language, type, actors
        } = req.body;

        const isMissing = checkMissingFields(req, res, { title, synopsis, rating, coverURL, releaseDate, seasons, author,
            genres, country, duration, language, type, actors });
        if (!isMissing) return;

        // Create the object to send to the database
        const movieToUpdate = {
            title, synopsis, rating, coverURL, releaseDate, seasons,
            author, genres, country, duration, language, type, actors
        };

        // Update the database with the new movie/series data
        const updatedMovie = await Movie.findByIdAndUpdate(Id, movieToUpdate, { new: true });

        // Check if the update was successful
        if (!updatedMovie) {
            return res.status(404).json({ success: false, message: "No movie/series with this ID found in the database." });
        }

        return res.status(201).json({ success: true, message: "The movie/series has been successfully updated in the database." });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred while updating the movie/series." });
    }
};


// Function to delete a movie/series from the database based on the provided ID
exports.deleteMovie = async (req, res) => {
    try {
        // User authentication
        const isAuth = authenticateUserForArticle(req, res);
        if (!isAuth) return;

        // Retrieve the ID from the request parameters
        const { Id } = req.query;

        // Check for the presence of the ID in the request parameters
        if (!Id) {
            return res.status(400).json({ success: false, message: "The route requires an ID parameter." });
        }

        // Remove the movie/series from the database
        const movieRemovalResult = await Movie.findByIdAndDelete(Id);
        // const reserveRemovalResult = await findOneAndDelete({articleId: Id, userId: req.query.userId });

        // Check if the movie/series was found and removed
        if (!movieRemovalResult) {
            return res.status(404).json({ success: false, message: "No movie/series with this ID found in the database." });
        }

        return res.status(201).json({ success: true, message: "The movie/series has been successfully deleted from the database." });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred while deleting the movie/series." });
    }
};
