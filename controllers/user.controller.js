const { deleteReserveByUser } = require("./dbConnection");
const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const { sign} = require("jsonwebtoken");
const nodemailer = require('nodemailer');
const { ObjectId } = require("mongodb");

const { getMissingFields, checkMissingFields } = require("../utils/common.utils");
const { authenticateUser } = require("../Middleware/auth.middleware");

const saltRounds = 10;

const capitalizeFirstLetter = string => `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

// Validate an email address
function validateEmail(email) {
    const emailRegex = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
    return emailRegex.test(email);
}


function generateTokenForUser(userId) {
    const payload = { userId: userId };
    return sign(payload, process.env.JWT_SECRET, {expiresIn: '24h'});
}


// Validate a password
function validatePassword(password) {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).{8,}$/;
    return passwordRegex.test(password);
}


//fonction permet d'inscrire un utilisateur dans la base de données
exports.registerUser = async (req, res) => {
    try {
        const {
            firstname, lastname, address, city, codePostal, country, province,
            phone, email, password, dateOfBirth
        } = req.body;

        // Validate required fields
        if (
            firstname && lastname && validateEmail(email) && validatePassword(password) &&
            phone && province && address && city && codePostal && country && dateOfBirth
        ) {
            // Check if the email is already in use
            const emailExists = await User.findOne({ email });
            if (emailExists) {
                return res.status(409).json({
                    success: false,
                    message: "The email address is already used by another user."
                });
            }

            // Generate a new ObjectId for the user
            const userId = new ObjectId();

            // Hash the user's password
            const salt = await bcrypt.genSalt(saltRounds);
            const hashedPassword = await bcrypt.hash(password, salt);

            // Create user data object
            const userData = {
                _id: userId,
                numDossier: userId.toString(),
                lastname: capitalizeFirstLetter(lastname),
                firstname: capitalizeFirstLetter(firstname),
                phone,
                address,
                city,
                email,
                password: hashedPassword,
                codePostal,
                country,
                province,
                dateOfBirth,
                admin: false
            };

            // Create the user in the database
            const register = await User.create(userData);
            if (!register) {
                return res.status(500).json({
                    success: false,
                    message: "An error occurred while creating the user."
                });
            }

            // User creation successful
            return res.status(201).json({
                success: true,
                message: "Congratulations! Your account has been successfully created."
            });


        } else {
            // Determine which fields are empty


            const missingFields = getMissingFields({
                lastname, firstname, phone, address, city,
                codePostal, province, dateOfBirth
            });

            if (!validateEmail(email)) missingFields.push("email");
            if (!validatePassword(password)) missingFields.push("password");

            // Return 400 error with the missing fields
            return res.status(400).json({
                success: false,
                message: "Please fill in all required fields.",
                additionalInfo: `Make sure to provide the required information for the following fields: ${missingFields.join(", ")}`
            });
        }

    } catch (error) {
        return res.status(500).json({
            success: false,
            message: "An error occurred while creating the user. Please try again later."
        });
    }
};

// Function to handle user login and return a token to the user.
exports.loginUser = async (req, res) => {
    try {
        // Extracting the email and the password from the request body.
        const { email, password } = req.body;

        // Check if email and password are provided
        if (email && password) {
            const userDB = await User.findOne({ email: email });

            // User not found in the authentication system.
            if (!userDB) {
                return res.status(404).json({
                    success: false,
                    message: "User does not exist. Please check your email or create an account."
                });
            }

            const result = await bcrypt.compare(password, userDB.password);

            // Incorrect password provided.
            if (!result) {
                return res.status(403).json({
                    success: false,
                    message: "Incorrect password. Please try again."
                });
            }

            const token = generateTokenForUser(userDB.numDossier);

            return res.status(200).json({
                token: token,
                id: userDB.numDossier,
                name: `${userDB.lastname} ${userDB.firstname}`,
                admin: userDB.admin,
            });

        } else {
            // Email or password is missing
            return res.status(400).json({
                success: false,
                message: "Please provide an email and a password"
            });
        }
    } catch (error) {
        // Other unexpected errors.
        return res.status(500).json({
            success: false,
            message: "An error occurred while logging in."
        });
    }
};

// Function to get information of the logged-in user
exports.getUser = async (req, res) => {
    try {
        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Fetch user data from the database using the user's UID
        const { userId } = authRes;
        const userDB = await User.findById(userId);
        if (!userDB) return res.status(404).json({ success: false, message: "The user hasn't been found." });

        // Send the user information with a 200 status code
        return res.status(200).json(userDB);

    } catch (error) {
        // If an error occurs during database access or processing, return a 500 status code
        return res.status(500).json({
            success: false, message: "An error occurred while retrieving data."
        });
    }
};

// This function updates the user's password.
exports.update_Password = async (req, res) => {
    try {
        const {password, newPassword} = req.body;

        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Fetch user data from the database using the user's UID
        const { userId } = authRes;
        const userDB = await User.findOne({ numDossier: userId });
        if (!userDB) return res.status(404).json({ success: false, message: "The user hasn't been found." });

        if (!userDB) return res.status(404).json({success: false, message: "User not found."});

        // Compare the provided password with the stored password
        const passwordMatch = await bcrypt.compare(password, userDB.password);

        if (!passwordMatch) {
            return res.status(403).json({
                success: false, message: "Invalid password. Please try again."
            });
        }

        // Generate a salt and hash the new password
        const salt = await bcrypt.genSalt(saltRounds);
        const hashedPassword = await bcrypt.hash(newPassword, salt);

        // Update the user's password in the database
        const updated = await User.findOneAndUpdate({ numDossier: userId}, { password: hashedPassword });

        if (!updated) {
            return res.status(500).json({
                success: false, message: "An error occurred while updating the password."
            });
        }

        return res.status(201).json({
            success: true, message: "Password has been successfully updated."
        });

    } catch (error) {
        return res.status(500).json({
            success: false,
            message: "Internal server error. Unable to update the password.",
        });
    }
}


exports.newPasswordReset = async (req, res) => {
    const { newPassword, email } = req.body;

    try {

        // Find the user by their email
        const userDB = await User.findOne({ email: email });

        if (!userDB) return res.status(404).json({success: false, message: "User does not exist."});

        // Generate a salt and hash the new password
        const salt = await bcrypt.genSalt(saltRounds);
        const hashedPassword = await bcrypt.hash(newPassword, salt);

        // Update the user's password in the database
        const updated = await User.findOneAndUpdate(
            { numDossier: userDB._id.toString() },
            { password: hashedPassword }, // Provide the update object here
            { new: true } // Set { new: true } to return the updated document
        );

        if (!updated) {
            return res.status(500).json({success: false, message: "An error occurred while updating the password."});
        }

        return res.status(201).json({
            success: true, message: "Password has been successfully reset."
        });
    } catch (error) {
        return res.status(403).json({
            success: false, message: "Access denied. Please check your permissions and try again."
        });
    }
};


exports.resetPassword = async (req, res) => {
    try {
        const {email, code} = req.body;

        const transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'samylichine.iss@gmail.com',
                pass: 'rqqc abnz kxne bxmn'
            }
        });

        // Check if email is provided
        if (email === "") return res.status(400).send({success: false, message: "Please fill in all the required fields."});
        if (validateEmail(email) === false) return res.status(400).send({success: false, message: "Please provide a valid email address."});

        // Find the user by their email
        const userDB = await User.findOne({ email: email });
        if (!userDB) return res.status(404).json({ success: false, message: "The user hasn't been found." });

        const mailOptions = {
            from: `book of friends <bookoffriends.team@gmail.com>`,
            to: email,
            subject: 'Password Reset Confirmation',
            html: `
                <!DOCTYPE html>
                <html lang="en">
                
                    <head>
                        <style>
                            body {
                                margin: 0;
                                padding: 0;
                                background-color: #f8f8f8;
                                font-family: 'Segoe UI', Arial, sans-serif;
                                font-size: 14px;
                                color: #333333;
                            }
                    
                            /* Add more styles as needed for your specific content */
                            .email-container {
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                width: 50%;
                                background-color: #f8f8f8;
                                padding: 20px;
                            }
                    
                            .email-content {
                                border: 1px solid #ccc;
                                background-color: #fff;
                                padding: 0 20px;
                                margin-bottom: 20px;
                            }
                    
                            .confirmation-code {
                                padding: 8px 16px 0;
                                background-color: #FFF4CE;
                            }
                    
                            .confirmation-code strong {
                                font-size: 18px;
                            }
                        </style>
                        <title>
                            Password Reset Confirmation
                        </title>
                    </head>
                    
                    <body>
                        <div class="email-container columns">
                            <div class="email-content column is-half is-offset-one-quarter">
                                <p>Dear ${userDB.lastname} ${userDB.firstname},</p>
                                <p>We have received a request to reset your password for your Book of friends library account.</p>
                                <p>To confirm your identity and proceed with the password reset, please use the following confirmation code:</p>
                                <table class="confirmation-code">
                                    <tr>
                                        <td>Confirmation Code:</td>
                                    </tr>
                                    <tr>
                                        <td><strong>${code}</strong></td>
                                    </tr>
                                </table>
                                <p>If you did not request this password reset, please ignore this email, and your account will remain secure.</p>
                                <p>Please note that the confirmation code is valid for a limited time for security purposes.</p>
                                <p>Thank you for using our library. If you have any questions or need further assistance, please don't hesitate to contact our support team at <a href="mailto:samylichine.iss@gmail.com">samylichine.iss@gmail.com</a>.</p>
                            </div>
                        </div>
                    </body>
                
                </html>
            `
        };

        await transporter.sendMail(mailOptions);

        return res.status(201).send({
            success: true,
            message: "We've sent a verification NIP to your email address. Please check your inbox and enter" +
                "the code below to reset your password. If you don't see the email, please check your spam folder."
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            message: "An error occurred while processing the request."
        });
    }
};


// Permet de supprimer un utilisateur
exports.deleteUser = async (req, res) => {
    try {
        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Find and delete the user by their dossier number
        const { userId } = authRes;
        const deletedUser = await User.findOneAndDelete({ numDossier: userId });

        // Check if the user exists and was successfully deleted
        if (!deletedUser) {
            return res.status(404).json({
                success: false,
                message: "User not found or an error occurred while deleting the user."
            });
        }

        // Delete any related data associated with the user
        await deleteReserveByUser(userId);

        // Respond with success message after successful deletion
        return res.status(201).json({
            success: true,
            message: "User deleted successfully.",
        });

    } catch (error) {
        return res.status(500).json({
            success: false,
            message: "An error occurred while deleting the user.",
            error: error.message // Optionally, include the specific error message for debugging
        });
    }
};


// Allows updating the information of the connected user
exports.updateProfile = async (req, res) => {
    try {
        // Extract profile information from the request body
        const { lastname, firstname, address, city, codePostal, country, province, dateOfBirth, phone } = req.body;

        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        const { userId } = authRes;

        // Check if all the required fields are provided
        const isMissing = checkMissingFields(req, res, { lastname, firstname, address, city, codePostal, province, phone, dateOfBirth });
        if (!isMissing) return;

        // Find the user by their email
        const userDB = await User.findOne({ numDossier: userId });
        if (!userDB) return res.status(404).json({ success: false, message: "The user hasn't been found." });

        // Prepare updated user data with capitalized names
        const userToUpdate = {
            lastname: capitalizeFirstLetter(lastname),
            firstname: capitalizeFirstLetter(firstname),
            address, city, codePostal, province, dateOfBirth, country, phone
        }

        // Update the database with the new user data
        const updatedUser = await User.findByIdAndUpdate(userId, userToUpdate, { new: true });

        // Check if the update was successful
        if (!updatedUser) return res.status(500).json({ success: false, message: "An error occurred when updating. Please try later." });

        // Return success message after updating the profile
        return res.status(201).json({
            success: true, message: "Your informations has been updated.",
        });
    } catch (error) {
        // Handle specific error cases
        return res.status(500).json({
            success: false, message: "Error updating the user's profile.",
            error: error.message // Optionally, include the specific error message for debugging
        });
    }
};


// Function to update the user's email address
exports.updateEmail = async (req, res) => {
    try {

        const { email } = req.body;

        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        const { userId } = authRes;

        // Validate the new email format
        if (!validateEmail(email)) {
            return res.status(400).json({
                success: false,
                message: 'Please check or fill in the required fields.',
            });
        }

        const userDB = await User.findOne({ email: email });

        if (userDB) {
            return res.status(409).json({ success: false, message: "The entered email address is already in use." });
        }


        const updatedUser = await User.findByIdAndUpdate({ numDossier: userId }, { email: email }, { new: true });

        if (!updatedUser) {
            return res.status(500).json({
                success: false, message: 'Error updating the user.',
            });
        }

        // Return a success response if everything goes well
        return res.status(200).json({
            success: true,
            message: 'Email address updated successfully.',
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            message: 'Error updating the user.',
        });
    }
}
