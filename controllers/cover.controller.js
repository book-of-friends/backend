const { sendResponse } = require("../utils/common.utils");
const {initializeMongoDB} = require("./dbConnection");


exports.addCover = async (req, res) => {
    try {
        const { bucket } = await initializeMongoDB();

        const file = req.file;
        if (!file) return res.status(400).json({ error: 'No file provided' });

        const uploadStream = bucket.openUploadStream(file.originalname, {
            chunkSizeBytes: 1024 * 1024,
            metadata: {
                name: file.originalname,
                size: file.size,
                type: file.mimetype
            }
        });

        uploadStream.end(file.buffer);

        uploadStream.on('finish', () => {
            res.status(201).json({ fileId: uploadStream.id, filename: file.originalname });
        });

        uploadStream.on('error', (err) => {
            console.error("Error uploading cover:", err);
            res.status(500).json({ error: 'Internal Server Error' });
        });
    } catch (error) {
        console.error("Error adding cover:", error);
        return sendResponse(res, 500, false, "Internal Server Error");
    }
};

exports.getCover = async (req, res) => {
    try {
        const { filename } = req.params;
        const { bucket } = await initializeMongoDB();

        const downloadStream = bucket.openDownloadStreamByName(filename);
        downloadStream.pipe(res);

        downloadStream.on('error', (err) => {
            console.error("Error downloading cover:", err);
            res.status(500).json({ error: 'Internal Server Error' });
        });
    } catch (error) {
        console.error("Error getting cover:", error);
        return sendResponse(res, 500, false, "Internal Server Error");
    }
};
