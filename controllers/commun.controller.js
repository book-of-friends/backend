const nodemailer = require('nodemailer');

const Comment = require("../models/comment.model");
const Movie = require("../models/movie.model");
const Album = require("../models/album.model");
const Book = require("../models/book.model");

const { authenticateUser, authenticateUserForArticle} = require("../Middleware/auth.middleware");
const {ObjectId} = require("bson");

// Function that sends the provided comment to the administrator via email
async function sendCourriel(comment) {
    // Create a nodemailer transporter for sending emails
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'samylichine.iss@gmail.com',
            pass: 'rqqc abnz kxne bxmn'
        }
    });

    // Email configuration options
    const mailOptions = {
        from: 'book of friends <bookoffriends.team@gmail.com>',
        to: 'samylichine.iss@gmail.com',
        subject: 'Subscriber Comment',
        html: `
        <body>
            <p>Hello,
                <br>
                    <strong> Comment sent by a subscriber: </strong>
                <br>
            </p>
            <div style="margin-left: 20px">
                <br>
                    <strong> ${comment} </strong>
                <br>
            </div>
            <br>
                Thank you,
            <br>
                The Book Of Friends Team<br>
            <br>
        </body>
        `
    };

    // Send the email and await the response
    const response = await transporter.sendMail(mailOptions);

    // Return whether the email was accepted
    return response.accepted;
}

// Function that retrieves all documents from the database
exports.getAllData = async (req, res) => {
    try {
        // Retrieving data from the database in parallel
        const [books, movies, albums] = await Promise.all([
            Book.find(), Movie.find(), Album.find(),
        ]);

        // Transforming function for reuse
        const transformItem = (item) => ({
            id: item._id.toString(),
            title: item.title,
            author: item.author || "unknown",
            coverURL: item.coverURL,
            type: item.type || "Album",
            genres: item.genres?.map(genre => genre.value) || [],
            language: item.language
        });

        // Transforming all data
        const dataToSend = [...books, ...movies, ...albums].map(transformItem);

        // No items found
        if (dataToSend.length === 0) return res.status(404).json({ success: false, message: "No items found." });

        // Sending the transformed data as JSON
        return res.status(200).json(dataToSend);
    } catch (error) {
        // Log the error for debugging purposes
        console.error("Error retrieving items:", error);

        // Return a more detailed error message
        return res.status(500).json({ success: false, message: "An error occurred while retrieving items. Check the server logs for more details." });
    }
};

// Function that retrieves documents from the database by a single author
exports.getSimilarities = async (req, res) => {
    try {
        // Retrieve the "author" parameter from the request
        const { author } = req.query;

        // Check if the "author" parameter is provided
        if (!author) {
            return res.status(400).json({
                success: false, message: "The 'author' parameter is missing in the request."
            });
        }

        // Search for books, movies, and albums by the author in the database
        const [books, movies, albums] = await Promise.all([
            Book.find({ author: author }),
            Movie.find({ actors: { name: author } }),
            Album.find({ author: author }),
        ]);

        // Prepare the data to be sent in the response
        const dataToSend = [...books, ...movies, ...albums].map(item => ({
            id: item._id.toString(),
            title: item.title,
            coverURL: item.coverURL,
            author,
            type: item.type || "Album", // Use "Album" if the type is not defined
        }));

        // Send the data with a status of 200 (OK)
        return res.status(200).json(dataToSend);

    } catch (error) {
        // Handle errors that occurred during the retrieval of items
        console.error(error); // Log the error for debugging purposes
        return res.status(500).json({ success: false, message: "An error occurred while retrieving items." });
    }
};

// Function to add a comment to the database
exports.addComment = async (req, res) => {
    try {
        // Extracting the comment from the request body
        const { comment } = req.body;

        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Checking for the presence of the comment in the request body
        if (!comment) {
            return res.status(400).json({ success: false, message: "No comment provided in the request body." });
        }

        // Preparing the data to display
        const id = new ObjectId();
        const dataDisplay = { _id: id, comment };

        // Sending the comment via email
        const isCommentSent = await sendCourriel(comment);

        if (isCommentSent) {
            // Adding the comment to the database
            const isCommentAdded = await Comment.create(dataDisplay);

            if (isCommentAdded) {
                return res.status(201).json({ success: true, message: "Thank you for your comment." });
            } else {
                return res.status(500).json({ success: false, message: "An error occurred while adding the comment to the database." });
            }
        } else {
            return res.status(500).json({ success: false, message: "An error occurred while sending the comment via email." });
        }

    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred during the processing of the request." });
    }
};

// Function to retrieve all comments made by users
exports.getAllComments = async (req, res) => {
    try {
        const isAuthenticated = await authenticateUserForArticle(req, res);  // User authentication
        if (!isAuthenticated) return; // If authentication fails, return

        const comments = await Comment.find();
        const dataToSend = comments.map(comment => ({ comment: comment.comment }));

        // No comments found
        if (dataToSend.length === 0) return res.status(404).json({ success: false, message: "No comments to show for now." });

        return res.status(200).json(dataToSend);
    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while processing the data." });
    }
};
