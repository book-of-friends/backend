const nodemailer = require('nodemailer');
const { authenticateUser, authenticateUserForArticle } = require("../Middleware/auth.middleware");
const { ObjectId } = require('mongodb');

const Reservation = require("../models/reservation.model");
const Album = require("../models/album.model");
const Book = require("../models/book.model");
const Movie = require("../models/movie.model");
const User = require("../models/user.model");

// Function to email the person whose email is provided as a parameter with the data given as parameters
async function sendEmail({email, nom, userId, articleNumber, articleName, author}) {
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'samylichine.iss@gmail.com',
            pass: 'rqqc abnz kxne bxmn'
        }
    });

    const mailOptions = {
        from: `book of friends <bookoffriends.team@gmail.com>`,
        to: email,
        subject: 'Reserved Document Available',
        html: `
        <body>
            <p>Hello ${nom},
                <br>
                    One or more documents reserved under the name (${nom} / ${userId}) are ready for pickup. They are:
                <br>
            </p>
            <div style="margin-left: 20px">
                <br>
                    <strong>${articleName} - ${articleNumber} / ${author}</strong>
                <br>
            </div>
            <br>
                You have up to <strong>5 days</strong> to pick up the documents. They are arranged in <strong>alphabetical order</strong> on shelves located on the first floor.
            <br>
                If you have <strong>canceled</strong> your reservation, please <strong>disregard this email</strong>.
            <br>
                Thank you,
                The Book Of Friends Team<br>
            <br>
        </body>
        `
    };

    const response = await transporter.sendMail(mailOptions)
    return response.accepted;
}

// Function to cancel a reservation by removing it from the reservation collection
exports.cancelReservation = async (req, res) => {
    try {
        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Fetch user data from the database using the user's UID
        const { userId } = authRes;
        const { Id } = req.query;

        if (!Id ) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });

        const foundRes = await Reservation.findById(Id);
        if (!foundRes) return res.status(404).json({ success: false, message: "This reservation does not exist" });
        if (foundRes.userId !== userId) return res.status(403).json({ success: false, message: "You are not authorized to cancel this reservation" });

        if (foundRes.state === "Borrowed") return res.status(404).json({ success: false, message: "This reservation cannot be canceled because the document is already borrowed" });

        const canceledReservation = await Reservation.findByIdAndDelete(Id);

        if (canceledReservation) return res.status(201).json({ success: true, message: "Your reservation has been successfully canceled" });
        else return res.status(500).json({ success: false, message: "An error occurred while canceling the reservation" });

    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred" });
    }
}

// Function that retrieves all reservations made by a user
exports.findReservations = async (req, res) => {
    try {
        // User authentication
        const authRes = await authenticateUserForArticle(req, res);
        if (!authRes) return;

        // Fetch all reservations
        const reservations = await Reservation.find();
        console.log(reservations)

        if (reservations.length === 0) return res.status(404).json({ success: false, message: "No reservations made at the moment" });

        const dataToSend = await Promise.all(reservations.map(async (reservation) => {
            const user = await User.findById(reservation.userId);
            const userName = `${user.firstname} ${user.lastname}`;

            const articleTypes = [Book, Movie, Album];
            const article = await Promise.all(articleTypes.map(async (type) => {
                return type.findById(reservation.articleId);
            }));

            const newList = article.filter(item => item !== null);
            const selectedArticle = newList.find((a) => a._id.toString() === reservation.articleId);

            return {
                idRes: reservation._id.toString(),
                id: selectedArticle._id.toString(),
                author: selectedArticle.author,
                title: selectedArticle.title,
                articleNumber: reservation.articleNumber,
                user: {
                    id: user._id.toString(),
                    name: userName,
                },
                coverURL: selectedArticle.coverURL,
                type: reservation.type,
                state: reservation.state,
                dateBorrow: reservation.dateBorrow,
                dateReturn: reservation.dateReturn,
            };
        }));

        res.status(200).json(dataToSend);
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: "An error occurred" });
    }
};

// Function that allows the user to make a reservation
exports.makeReservation = async (req, res) => {
    try {
        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Fetch user data from the database using the user's UID
        const { userId } = authRes;

        const user = await User.findById(userId);

        const { Id, IdNum, Type } = req.query;

        if (!Id || !IdNum || !Type) return res.status(400).json({ success: false, message: "Check both parameters in the route" });

        let article;
        switch (Type) {
            case 'album':
                article = await Album.findById(Id);
                break;

            case 'book':
                article = await Book.findById(Id);
                break;

            default:
                article = await Movie.findById(Id);
                break;
        }
        if (!article) return res.status(404).json({ success: false, message: "This article does not exist" });

        const findRes = await Reservation.findOne({ articleId: Id, articleNumber: IdNum });

        if (findRes) {
            if (findRes.state === "Borrowed") return res.status(404).json({ success: true, message: `This work is already borrowed, it will be available again on ${findRes.dateReturn}` });
            else if (findRes.state === "Reserved") return res.status(404).json({ success: true, message: `This work is already reserved by another person` });
            else if (findRes.state === "Available" || findRes.state === "Canceled") {
                const reservationWaiting = {
                    articleId: Id,
                    userId: userId,
                    articleNumber: IdNum,
                    type: Type,
                    state: "Reserved"
                };

                const reservationUpdate = Reservation.findOneAndUpdate({ _id: Id, articleNumber: IdNum }, reservationWaiting, { new: true });

                setTimeout(
                    async () => {
                        const userSend = {
                            email: user.email,
                            nom: user.lastname + " " + user.firstname,
                            userId: user._id.toString(),
                            articleNumber: IdNum,
                            articleName: article.title,
                            coverURL: article.coverURL,
                            author: article.author,
                            type: Type,
                        };

                        const emailResponse = await sendEmail(userSend);

                        if (emailResponse) {
                            const reservationApproved = {
                                articleId: Id,
                                userId: user._id.toString(),
                                articleNumber: IdNum,
                                type: Type,
                                dateBorrow: new Date().toISOString().split('T')[0],
                                dateReturn: new Date(new Date().setDate(new Date().getDate() + 15)).toISOString().split('T')[0],
                                state: "Borrowed"
                            };

                            await Reservation.findOneAndUpdate({ _id: Id, articleNumber: IdNum }, reservationApproved, { new: true });
                        }
                    }, 200000
                );

                if (reservationUpdate) return res.status(201).json({ success: true, message: "Please wait to receive an email confirming that the reserved document is ready for you before going to the library." });
                else return res.status(500).json({ success: false, message: "An error occurred" });
            }
        } else if (!findRes) {
            const reservationWaiting = {
                articleId: Id,
                userId: user.numDossier,
                author: article.author,
                type: Type,
                coverURL: article.coverURL,
                articleNumber: IdNum,
                articleName: article.title,
                state: "Reserved"
            };

            const id = new ObjectId();
            const response = await Reservation.create({ _id: id, ...reservationWaiting });

            setTimeout(
                async () => {
                    const userSend = {
                        email: user.email,
                        nom: user.firstname + " " + user.lastname,
                        userId: user.numDossier,
                        articleNumber: IdNum,
                        coverURL: article.coverURL,
                        articleName: article.title,
                        author: article.author,
                        type: Type,
                    };
                    const emailResponse = await sendEmail(userSend);

                    if (emailResponse) {
                        console.log(article)
                        const reservationApproved = {
                            articleId: Id,
                            userId: user.numDossier,
                            type: Type,
                            articleNumber: IdNum,
                            dateBorrow: new Date().toISOString().split('T')[0],
                            dateReturn: new Date(new Date().setDate(new Date().getDate() + 15)).toISOString().split('T')[0],
                            state: "Borrowed"
                        };

                        await Reservation.findOneAndUpdate({ articleId: Id, articleNumber: IdNum }, reservationApproved, { new: true });
                    }
                }, 200000
            );

            if (response) return res.status(201).json({ success: true, message: "Please wait to receive an email confirming that the reserved document is ready for you before going to the library." });
            else return res.status(500).json({ success: false, message: "An error occurred" });
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred" });
    }
};


const getTypeArticleString = (type, article, reservation) => {
    if (type === 'Album') return `${article.title}`;
    else return `${article.title} - ${reservation.articleNumber || ''}`;
};

exports.getReservation = async(req,res) => {
    try {
        // User authentication
        const authRes = await authenticateUser(req, res);
        if (!authRes.success) return;

        // Fetch user data from the database using the user's UID
        const { userId } = authRes;
        const reservations = await Reservation.find({ userId });

        if (reservations.length === 0) return res.status(200).json({ success: false, message: "You have no reservations" });
        const listToReturn = await Promise.all(
            reservations.map(async (reservation) => {
                let article, type;

                switch (reservation.type) {
                    case 'album':
                        article = await Album.findById(reservation.articleId);
                        type = 'Album';
                        break;
                    case 'book':
                        article = await Book.findById(reservation.articleId);
                        type = 'Book';
                        break;
                    default:
                        article = await Movie.findById(reservation.articleId);
                        type = 'Movie';
                }

                return {
                    idReservation: reservation._id,
                    articleID: article._id,
                    author: article.author,
                    article: getTypeArticleString(type, article, reservation),
                    dateBorrow: reservation.dateBorrow,
                    dateReturn: reservation.dateReturn,
                    state: reservation.state,
                    type,
                    coverURL: article.coverURL,
                };
            })
        );

        return res.status(200).json( listToReturn );

    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred" });
    }
};
