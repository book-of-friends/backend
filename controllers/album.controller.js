const Album = require("../models/album.model");
const Reservation = require('../models/reservation.model');

const { authenticateUserForArticle } = require("../Middleware/auth.middleware");
const { checkMissingFields } = require("../utils/common.utils");
const {ObjectId} = require("bson");

// Function for adding an album to the database
exports.addAlbum = async (req, res) => {
    try {
        const isAuthenticated = await authenticateUserForArticle(req, res);  // User authentication
        if (!isAuthenticated) return; // If authentication fails, return

        // Destructuring request body
        const { title, releaseDate, author, tracks, label, coverURL, language, albumState } = req.body;

        // Checking for missing fields
        const hasMissingFields = checkMissingFields(req, res, { title, releaseDate, author, tracks, label, coverURL, language, albumState });
        if (!hasMissingFields) return; // If there are missing fields, return

        // Creating album object to send to the database
        const id = new ObjectId();
        const albumToSend = { _id: id, title, releaseDate, author, label, albumState, coverURL, tracks, language };

        // Adding album data to the database
        const response = await Album.create(albumToSend);

        // If adding to the database fails, return an error response
        if (!response) return res.status(500).json({ success: false, message: 'An error occurred while adding the album.' });

        // If successful, return a success response
        return res.status(201).json({ success: true, message: 'The album has been added to the list.' });

    } catch (error) {
        console.error("Error in addAlbum:", error);
        // Handle any unexpected errors
        return res.status(500).json({ success: false, message: 'An error occurred.' });
    }
};


//Function to retrieve all albums from the database
exports.findAlbums = async(req,res) =>{
    try {
        const albums = await Album.find();

        if (albums.length > 0) {
            const data = albums.map(album => ({
                id: album._id.toString(),
                title: album.title,
                author: album.author || "unknown",
                coverURL: album.coverURL,
                type: "Album",
                language: album.language,
            }));

            return res.status(200).json(data);
        } else {
            return res.status(404).json({ success: false, message: "No albums found." });
        }
    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while retrieving albums." });
    }
};

// Function to retrieve an album from the database
exports.findAlbum = async (req, res) => {
    try {
        // Retrieve the album's identifier from the request
        const { Id } = req.query;
        if (!Id) return res.status(400).json({ success: false, message: "The route requires a parameter" });

        // Search for the album in the database
        const album = await Album.findById(Id);

        // If no album is found, return a 404 response
        if (!album) return res.status(404).json({ success: false, message: "No album found." });

        // Get the album's availability information
        const reservation = await Reservation.find({ articleId: Id});
        const albumAvailable = reservation[0];

        let availability = "Available";

        if (albumAvailable) {
            availability = albumAvailable.state === "Borrowed"
                ? `${albumAvailable.state} - Return expected on ${albumAvailable.dateReturn}`
                : albumAvailable.state;
        }

        // Prepare the data to be sent in the response
        const responseData = {
            id: album._id.toString(),
            title: album.title,
            releaseDate: album.releaseDate,
            label: album.label,
            coverURL: album.coverURL,
            albumState: availability,
            tracks: album.tracks,
            language: album.language,
            author: album.author || "Unknown",
        };

        // Return the response with the album's data
        return res.status(200).json(responseData);
    } catch (error) {
        console.error("Error in findAlbum:", error);
        // In case of an error, return a 500 error response
        res.status(500).json({ success: false, message: "An error occurred while retrieving the album." });
    }
};


//fonction qui permet la suppression dans la base de données
exports.deleteAlbum = async(req,res) =>{
    try {
        // User authentication
        const isAuth = authenticateUserForArticle(req, res);
        if (!isAuth) return; // Return early if authentication fails

        // Retrieve the ID from the request parameters
        const { Id } = req.query;

        // Check for the presence of the ID in the request parameters
        if (!Id) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });

        // Remove the album from the database
        const albumRemovalResult = await Album.findByIdAndDelete(Id);
        // const reserveRemovalResult = await findOneAndDelete({articleId: Id, userId: req.query.userId });

        // Check if the album was not found in the database
        if (!albumRemovalResult) {
            return res.status(404).json({ success: false, message: "No album with this ID found in the database." });
        }

        // Respond with success message
        return res.status(201).json({ success: true, message: "The album has been successfully deleted from the database." });
    } catch (error) {
        // Handle unexpected errors
        console.error("Error in deleteAlbum:", error);
        return res.status(500).json({ success: false, message: "An error occurred." });
    }
};


//fonction qui permet la modification d'un album dans la base de données
exports.updateAlbum = async(req,res) =>{
    try {
        // User authentication
        const isAuth = authenticateUserForArticle(req, res);
        if (!isAuth) return; // Return early if authentication fails

        // Retrieve the ID from the request parameters
        const { Id } = req.query;

        // Check for the presence of the ID in the request parameters
        if (!Id) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });


        const { title, releaseDate, author, label, coverURL, tracks, language } = req.body;

        // Checking for missing fields
        const hasMissingFields = checkMissingFields(req, res, { title, releaseDate, author, label, coverURL, tracks, language });
        if (!hasMissingFields) return; // If there are missing fields, return

        const albumToSend = { title, releaseDate, author, label, coverURL, tracks, language };

        const updatedAlbum = await Album.findByIdAndUpdate(Id, albumToSend, { new: true });

        // Check if the update was successful
        if (!updatedAlbum) return res.status(404).json({ success: false, message: "No album with this ID found in the database." });
        return res.status(201).json({ success: true, message: "The album has been successfully updated in the database." });

    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while updating the album." });
    }
};
