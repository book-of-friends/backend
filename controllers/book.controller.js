const { authenticateUserForArticle } = require("../Middleware/auth.middleware");
const {checkMissingFields} = require("../utils/common.utils");
const Book = require("../models/book.model");
const Reservation = require("../models/reservation.model");
const {ObjectId} = require("mongodb");

const BOOK_TYPES = {
    ROMAN: "Roman", MANGA: "Manga",
};


// Function for adding a book to the database
exports.addBook = async(req,res) =>{
    try {
        // // User authentication
        // const isAuth = await authenticateUserForArticle(req, res);
        // if (!isAuth) return;

        const { title, ISBN, plot, author, releaseDate, nbrPages, coverURL, type, genres, language, bookState } = req.body;

        const isMissing = checkMissingFields(req, res, { title, ISBN, plot, author, releaseDate, nbrPages, coverURL, type, language, bookState, genres });
        if (!isMissing) return;

        // Check if a book with the same ISBN already exists
        const existingBook = await Book.findOne({ ISBN: ISBN });
        if (existingBook) return res.status(409).json({ success: false, message: 'A book with the same ISBN already exists.' });

        // Prepare book data for insertion
        const id = new ObjectId();
        const bookToSend = {
            _id: id, ISBN, title, plot, author, bookState,
            coverURL, nbrPages, releaseDate, type, genres, language,
        };

        // ajout de données
        const response = await Book.create(bookToSend);
        console.log("response");


        if (!response)  return res.status(500).json({ success : false, message: 'An error occurred while adding the book.' });
        return res.status(201).json({ success : true, message: 'The book has been added to the list.' })

    } catch (error) {
        console.error('Error adding book:', error);
        return res.status(500).json({ success: false, message: 'An error occurred.' });
    }
};

//Function to delete a book from the database
exports.deleteBook = async(req,res) =>{
    try {
        // User authentication
        const isAuth = await authenticateUserForArticle(req, res);
        if (!isAuth) return;

        // Retrieve the ID from the request parameters
        const { Id } = req.query;

        // Check for the presence of the ID in the request parameters
        if (!Id) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });

        // Remove the movie/series from the database
        const bookRemovalResult = await Book.findByIdAndDelete(Id);
        // const reserveRemovalResult = await Reservation.findOneAndDelete({articleId: Id, userId: req.query.userId });

        // Check if the movie/series was found and removed
        if (!bookRemovalResult) {
            return res.status(404).json({ success: false, message: "No book with this ID found in the database." });
        }

        return res.status(201).json({ success: true, message: "The book has been successfully deleted from the database." });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred" });
    }
}

const transformBookData = (books) => {
    return books.map((book) => ({
        id: book._id.toString(),
        title: book.title,
        author: book.author || "Unknown",
        coverURL: book.coverURL,
        type: book.type,
        genres: book.genres.map(genre => genre.value),
        language: book.language
    }));
};

//Function to retrieve all books from the database
exports.findBooks = async(req,res) =>{
    try {
        // Retrieve all books from the database
        const books = await Book.find();

        if(books.length > 0) {
            // Transform the data for display
            const bookToDisplay = transformBookData(books);
            return res.status(200).json( bookToDisplay );
        } else {
            return res.status(404).json({ success: false, message: "No books found" });
        }
    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while retrieving data" });
    }
}

// Function to retrieve all mangas from the database
exports.findMangas = async(req,res) =>{
    try {
        const mangas = await Book.find({type: BOOK_TYPES.MANGA});

        if(mangas.length > 0) {
            // Transform the mangas for display
            const mangaToDisplay = transformBookData(mangas);
            return res.status(200).json( mangaToDisplay );
        } else {
            return res.status(404).json({ success: false, message: "No mangas found" });
        }
    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while retrieving data" });
    }
}

// Function to retrieve all novels from the database
exports.findRomans = async(req,res) =>{
    try {
        const romans = await Book.find({type: BOOK_TYPES.ROMAN});

        if(romans.length > 0) {
            // Transform the novels for display
            const romanToDisplay = transformBookData(romans);
            return res.status(200).json( romanToDisplay );
        } else {
            return res.status(404).json({ success: false, message: "No novels founds" });
        }
    } catch (error) {
        return res.status(500).json({ success: false, message: "An error occurred while retrieving data" });
    }
}


function handleRomanBook(reservations, volumes) {
    const volumeInfo = reservations.length === 0
        ? { volume: "v.1", availability: "Available" }
        : (reservations[0].state === "Borrowed")
            ? { volume: "v.1", availability: `${reservations[0].state} - Will be returned on ${reservations[0].dateReturn}` }
            : { volume: "v.1", availability: reservations[0].state };

    volumes.push(volumeInfo);
}

function handleMangaBook(book, reservations, volumes) {
    const volumeNumbers = [...Array(parseInt(book.nbrPages)).keys()];

    volumeNumbers.forEach((volumeNumber) => {
        const articleNumber = `v.${volumeNumber + 1}`;
        const reservation = reservations.find((r) => r.articleNumber === articleNumber);

        const volumeInfo = reservation ? (reservation.state === "Borrowed")
                ? { volume: articleNumber, availability: `${reservation.state} - Will be returned on ${reservation.dateReturn}` }
                : { volume: articleNumber, availability: reservation.state }
            : { volume: articleNumber, availability: "Available" };

        volumes.push(volumeInfo);
    });
}

function formatBookData(book, volumes) {
    return {
        id: book._id,
        title: book.title,
        author: book.author || "unknown",
        plot: book.plot,
        ISBN: book.ISBN,
        nbrPages: book.nbrPages,
        releaseDate: book.releaseDate,
        coverURL: book.coverURL,
        bookState: volumes,
        type: book.type,
        genres: book.genres,
        language: book.language,
    };
}


/*
    Function that retrieves a book from the database based on the provided ID parameter.
    Depending on the document type, it retrieves the necessary information,
    and based on the season reservations, manipulates the data for display.
*/
exports.findBook = async(req,res) =>{
    try {
        const { Id } = req.query;
        if (!Id) return res.status(400).json({ success: false, message: "The route requires a parameter" });

        const book = await Book.findById(Id);

        if(!book) return res.status(404).json({ success: false, message: "No book has been found." });

        const bookRes = await Reservation.find({ articleId: Id});

        const volumes = [];

        if (book.type === BOOK_TYPES.ROMAN) handleRomanBook(bookRes, volumes)

        if (book.type === "Manga") handleMangaBook(book, bookRes, volumes);

        const dataSend = formatBookData(book, volumes);

        return res.status(200).json(dataSend);
    } catch (error) {
        return res.status(500).json({ success: false, message: "une erreur lors de la récupération des livres" });
    }
}

//Function for updating a book's information in the database
exports.updateBook = async(req,res) =>{
    try {
        // User authentication
        const isAuth = await authenticateUserForArticle(req, res);
        if (!isAuth) return;

        // Retrieve the ID from the request parameters
        const { Id } = req.query;
        if (!Id) return res.status(400).json({ success: false, message: "The route requires an ID parameter." });

        const { ISBN, title, plot, author, releaseDate, coverURL, type, genres, language } = req.body;

        const isMissing = checkMissingFields(req, res, { ISBN, title, plot, author, releaseDate, coverURL, type, genres, language });
        if (!isMissing) return;

        const bookToUpdate = {
            title, plot, author: author || "unknown", releaseDate,
            coverURL, type, genres, language
        };

        const updatedBook = await Book.findByIdAndUpdate(Id, bookToUpdate, { new: true });

        if (!updatedBook) return res.status(404).json({ success: false, message: "No book with this ID found in the database." });
        return res.status(201).json({ success: true, message: 'The book has been updated successfully.' });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: "An error occurred." });
    }
};
