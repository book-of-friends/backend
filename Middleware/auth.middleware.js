// Middleware function for user authentication
const { sendResponse } = require("../utils/common.utils");
const { verify } = require('jsonwebtoken');
const User = require('../models/user.model');

exports.authenticateUser = async (req, res) => {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        sendResponse(res, 401, false, "Authentication failed. Please log in to continue.");
        return { success: false };
    }

    const token = authHeader.split(' ')[1];
    const decoded = verify(token, process.env.JWT_SECRET);
    return { success: true, userId: decoded.userId};
};


exports.authenticateUserForArticle = async (req, res) => {
    const authRes = await exports.authenticateUser(req, res);

    if (!authRes.success) return false;

    const { userId } = authRes;
    const userInfo = await User.findById(userId);

    // Check if the user is authorized to add a movie/series
    if (!userInfo.admin) {
        sendResponse(res, 403, false, "You are not authorized to add movies/series.");
        return false;
    }

    return true;
};
