module.exports = {
    // operation's method
    get: {
      tags: ["Movies"], // operation's tag.
      summary: "affiche les informations d'un seul film", // operation's desc.
      operationId: "getSingleBook", // unique operation email
      parameters: [
        {
            $ref: '#/components/parameters/Id' // data model of the param
        },
      ], // expected params.
      // expected responses
      responses: {
        // response code
        200: {
          description: "les données d'un film sont retournées", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/FilmResponse", // user data model
              },
            },
          },
        },
        // response code
        404: {
          description: "réponse si aucun film n'est trouvé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        500: {
          description: "réponse si le serveur a rencontré une situation qu'il ne sait pas gérer", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
      },
    },
  };