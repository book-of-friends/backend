module.exports = {
    // method of operation
    delete: {
      tags: ["Movies"], // operation's tag.
      security: [
        {
          bearerAuth: []
        }
      ],
      summary: "Route pour supprimer un film de la base de données", // operation's desc.
      operationId: "deleteBook", // unique operation id.
      parameters: [
        {
            $ref: '#/components/parameters/Id' // data model of the param
        },
      ], // expected params.
      // expected responses
      responses: {
        // response code
        201: {
          description: "La réponse lorsque le film est bien supprimé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/SuccessMessage", // User model
              },
            },
          },
        },
        // response code
        401: {
            description: "réponse si l'utilisateur n'est pas connecté", // response desc.
            content: {
              // content-type
              "application/json": {
                schema: {
                  $ref: "#/components/schemas/ErrorMessage", // user data model
                },
              },
            }
        },
        // response code
        403: {
            description: "réponse si l'utilisateur n'est pas un administrateur", // response desc.
            content: {
              // content-type
              "application/json": {
                schema: {
                  $ref: "#/components/schemas/ErrorMessage", // user data model
                },
              },
            },
          },
        // response code
        404: {
          description: "réponse si aucun film n'est trouvé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        400: {
          description: "réponse si le paramétre est invalide ou il manque de données pour l'ajout", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        500: {
          description: "réponse si le serveur a rencontré une situation qu'il ne sait pas gérer", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
      },
    },
  };