module.exports = {
    post: {
        tags: ["All products"], // Operation's tag.
        summary: "Upload a cover file", // Operation's description.
        operationId: "uploadCover", // Unique operation id.
        requestBody: {
            required: true, // Mandatory param
            content: {
                "multipart/form-data": {
                    schema: {
                        type: "object",
                        properties: {
                            file: {
                                type: "string",
                                format: "binary",
                            },
                        },
                    },
                },
            },
        },
        responses: {
            201: {
                description: "Successfully uploaded",
                content: {
                    "application/json": {
                        schema: {
                            type: "object",
                            properties: {
                                fileId: {
                                    type: "string",
                                },
                            },
                        },
                    },
                },
            },
            400: {
                description: "Bad Request",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage",
                        },
                    },
                },
            },
            500: {
                description: "Internal Server Error",
                content: {
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage",
                        },
                    },
                },
            },
        },
    },
};
