module.exports = {
    // Operation's method
    get: {
        tags: ["Comments"], // Operation's tag.
        security: [
            {
                bearerAuth: []
            }
        ],
        summary: "List of comments", // Operation's description.
        operationId: "getAllComments", // Unique operation identifier.
        // Expected responses
        responses: {
            // Response code
            200: {
                description: "List of comments returned", // Response description.
                content: {
                    // Content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/CommentResponse", // User data model
                        },
                    },
                },
            },
            // Response code
            404: {
                description: "Response if no comments are found", // Response description.
                content: {
                    // Content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // Error data model
                        },
                    },
                },
            },
            // Response code
            401: {
                description: "Response if the user is not authenticated", // Response description.
                content: {
                    // Content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // User data model
                        },
                    },
                },
            },
            // Response code
            403: {
                description: "Response if the user is not authorized", // Response description.
                content: {
                    // Content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // User data model
                        },
                    },
                },
            },
            // Response code
            500: {
                description: "Response if the server encounters an unhandled situation", // Response description.
                content: {
                    // Content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // Error data model
                        },
                    },
                },
            },
        },
    },
};
