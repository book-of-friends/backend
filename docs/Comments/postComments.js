module.exports = {
  // Operation's method
  post: {
    tags: ["Comments"], // Operation's tag.
    summary: "Add a comment", // Operation's description.
    operationId: "postComment", // Unique operation identifier
    requestBody: {
      required: true, // Mandatory parameter
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/CommentPayload", // User data model
          },
        },
      },
    },
    // Expected responses
    responses: {
      // Response code
      200: {
        description: "Success message returned", // Response description.
        content: {
          // Content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/SuccessMessage", // User data model
            },
          },
        },
      },
      // Response code
      404: {
        description: "Response if no comment is found", // Response description.
        content: {
          // Content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // Error data model
            },
          },
        },
      },
      // Response code
      401: {
        description: "Response if the user is not authenticated", // Response description.
        content: {
          // Content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // User data model
            },
          },
        },
      },
      // Response code
      500: {
        description: "Response if the server encounters an unhandled situation", // Response description.
        content: {
          // Content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // Error data model
            },
          },
        },
      },
    },
  },
};
