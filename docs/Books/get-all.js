module.exports = {
    // operation's method
    get: {
      tags: ["All products"], // operation's tag.
      summary: "liste des articles", // operation's desc.
      operationId: "getAllProducts", // unique operation email
      // expected responses
      responses: {
        // response code
        200: {
          description: "les données des articles sont retournées", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BookLink", // user data model
              },
            },
          },
        },
        // response code
        404: {
          description: "réponse si aucun article n'est trouvé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        500: {
          description: "réponse si le serveur a rencontré une situation qu'il ne sait pas gérer", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
      },
    },
  };