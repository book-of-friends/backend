module.exports = {
    // operation's method
    get: {
      tags: ["Books"], // operation's tag.
      summary: "affiche les informations d'un seul livre", // operation's desc.
      operationId: "getSingleBook", // unique operation email
      parameters: [
        {
            $ref: '#/components/parameters/Id' // data model of the param
        },
      ], // expected params.
      // expected responses
      responses: {
        // response code
        200: {
          description: "les données d'un livre sont retournées", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/BookResponse", // user data model
              },
            },
          },
        },
        // response code
        400: {
          description: "réponse si le paramétre est invalide ou il manque de données pour l'ajout", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        404: {
          description: "réponse si aucun livre n'est trouvé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        500: {
          description: "réponse si le serveur a rencontré une situation qu'il ne sait pas gérer", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
      },
    },
  };