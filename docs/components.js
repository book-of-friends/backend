module.exports = {
    components: {
        securitySchemes: {
            bearerAuth: {
                type: "http",
                scheme: "bearer",
                bearerFormat: "JWT"
            }
        },
        parameters: {
            // Parameters for article identification
            Id: {
                name: "Id", // Parameter name
                type: "query", // Parameter type
                description: "Article ID", // Parameter description
                in: "query", // Parameter location
                schema: {
                    type: "string", // Example data
                    example: "978220729310", // Example data
                },
            },
            IdNum: {
                name: "IdNum", // Parameter name
                type: "query", // Parameter type
                description: "Season/Volume number", // Parameter description
                in: "query", // Parameter location
                schema: {
                    type: "string", // Example data
                    example: "v.1", // Example data
                },
            },
            Type: {
                name: "Type", // Parameter name
                type: "query", // Parameter type
                description: "Article type", // Parameter description
                in: "query", // Parameter location
                schema: {
                    type: "string", // Example data
                    example: "books", // Example data
                },
            },
            author: {
                name: "author", // Parameter name
                type: "query", // Parameter type
                description: "Author's last name", // Parameter description
                in: "query", // Parameter location
                schema: {
                    type: "string", // Example data
                    example: "Yoasobi", // Example data
                },
            },
            IdTrack: {
                name: "IdTrack", // Parameter name
                type: "query", // Parameter type
                description: "Track ID", // Parameter description
                in: "query", // Parameter location
                schema: {
                    type: "string", // Example data
                    example: "tt0111161", // Example data
                },
            },
        },

        schemas: {
            // Token creation payload schema
            TokenCreationPayload: {
                type: "object", // Data type
                required: ["email", "password"], // Required fields
                properties: {
                    email: { type: "string", },
                    password: { type: "string", },
                },
                example: {
                    email: "samylichine.iss@gmail.com", // Example email
                    password: "Passw0rd@", // Example password
                },
            },

            // Token creation response schema
            TokenCreationResponse: {
                type: "object", // Data type
                required: ["token", "id", "name"], // Required fields
                properties: {
                    token: { type: "string", },
                    id: { type: "string", },
                    name: { type: "string", },
                },
                example: {
                    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyaWQiOjIsIm5hbWUiOiJNYXJpZSBDdXJpZSIsImVtYWlsIjoibWFjdXJpZUBzY2llbmNlLmNvbSIsImlhdCI6MTYxOTIyNjkxNn0.Nn0SP4ZzW4jaOu_Q47Cq-NPm545zfxJmY7ww7GWyJL0", // Example token
                    id: "J8IYJrPbUE7qF5QCs4H83pYCA2", // Example ID
                    name: "Samy Issiakhem", // Example name
                },
            },

            // User creation payload schema
            UserCreationPayload: {
                type: "object", // Data type
                required: ["lastname", "firstname", "address", "city", "codePostal", "country", "province", "phone", "email", "password", "dateOfBirth"], // Required fields
                properties: {
                    lastname: { type: "string", },
                    firstname: { type: "string", },
                    address: { type: "string", },
                    city: { type: "string", },
                    codePostal: { type: "string", },
                    country: { type: "string", },
                    province: { type: "string", },
                    phone: { type: "string", },
                    email: { type: "string", },
                    password: { type: "string", },
                    dateOfBirth: { type: "string", },
                },
                example: {
                    lastname: "Issiakhem",
                    firstname: "Samy",
                    address: "123 Rue de la Paix",
                    city: "Montreal",
                    codePostal: "H3H 3H3",
                    country: "Canada",
                    province: "Quebec",
                    phone: "514-123-4567",
                    email: "samylichine.iss@gmail.com",
                    password: "Passw0rd@",
                    dateOfBirth: "1990-01-01",
                },
            },

            // Model for similarity response
            SimilarityResponse: {
                type: "object", // Data type
                required: ["id", "title", "coverURL", "author", "type"], // Required fields
                properties: {
                    id: { type: "string", },
                    title: { type: "string", },
                    coverURL: { type: "string", },
                    author: { type: "string", },
                    type: { type: "string", }
                },
                example: {
                    id: "1",
                    title: "The Jungle Book",
                    coverURL: "https://books.google.ca/books/publisher/content?id=43g9DgAAQBAJ&hl=en&pg=PP1&img=1&zoom=3&bul=1&sig=ACfU3U1-FoM1Y6Z6DTzED4CfFGMEbr-tuA&w=1280",
                    author: "Rudyard Kipling",
                    type: "Book",
                },
            },

            // Model for password reset
            PasswordReset: {
                type: "object", // Data type
                required: ["email", "code"], // Required fields
                properties: {
                    email: { type: "string", },
                    code: { type: "string", },
                },
                example: {
                    email: "samylichine.iss@gmail.com",
                    code: "123456"
                }
            },

            // Model for user update payload
            UserUpdatePayload: {
                type: "object", // Data type
                required: ["lastname", "firstname", "address", "city", "codePostal", "country", "province", "phone", "dateOfBirth"], // Required fields
                properties: {
                    lastname: { type: "string", },
                    firstname: { type: "string", },
                    address: { type: "string", },
                    city: { type: "string", },
                    codePostal: { type: "string", },
                    country: { type: "string", },
                    province: { type: "string", },
                    phone: { type: "string", },
                    dateOfBirth: { type: "string", },
                },
                example: {
                    lastname: "Issiakhem",
                    firstname: "Samy",
                    address: "123 Rue de la Paix",
                    city: "Montreal",
                    codePostal: "H3H 3H3",
                    country: "Canada",
                    province: "Quebec",
                    phone: "514-123-4567",
                    dateOfBirth: "1990-01-01",
                },
            },



            // Model for password input
            PasswordPayload: {
                type: "object", // Data type
                required: ["password", "newPassword"], // Required fields
                properties: {
                    password: { type: "string", },
                    newPassword: { type: "string", },
                },
                example: {
                    password: "Passw0rd@",
                    newPassword: "E1234567-89",
                },
            },

            // Model for email input
            EmailPayload: {
                type: "object", // Data type
                required: ["email"], // Required fields
                properties: {
                    email: { type: "string", },
                },
                example: {
                    email: "samylichine.iss@gmail.com"
                },
            },


            // Model for film input payload
            FilmPayload: {
                type: "object", // Data type
                required: ["title", "synopsis", "actors", "language", "author", "rating", "releaseDate", "seasons", "coverURL", "episodes", "country", "duration", "movieState", "type", "genres"], // Required fields
                properties: {
                    title: { type: "string" },
                    synopsis: { type: "string" },
                    rating: { type: "string" },
                    coverURL: { type: "string" },
                    seasons: { type: "string" },
                    episodes: { type: "string" },
                    language: { type: "string" },
                    releaseDate: { type: "string" },
                    country: { type: "string" },
                    duration: { type: "string" },
                    movieState: { type: "string" },
                    type: { type: "string" },
                    genres: { type: "array", items: { type: "string" } },
                    actors: { type: "array", items: { type: "string" } },
                    author: { type: "string" }
                },
                example: {
                    title: "Crash Landing on You",
                    synopsis: "Crash Landing on You is an American film directed by Jon Favreau, released in 2016. It is an adaptation of Rudyard Kipling's novel, published in 1894. The film is produced by Walt Disney Pictures and distributed by Walt Disney Studios Motion Pictures. It was released in France on October 26, 2016.",
                    rating: "5",
                    coverURL: "https://m.media-amazon.com/images/M/MV5BMzRiZWUyN2YtNDI4YS00NTg2LTg0OTgtMGI2ZjU4ODQ4Yjk3XkEyXkFqcGdeQXVyNTI5NjIyMw@@._V1_.jpg",
                    episodes: "16",
                    country: "USA",
                    seasons: 1,
                    language: "Korean",
                    releaseDate: "2016-01-01",
                    duration: "1h 45min",
                    movieState: "Available",
                    type: "Film",
                    genres: [
                        {
                            "value": "Romance",
                            "label": "Romance"
                        },
                        {
                            "value": "Drame",
                            "label": "Drame"
                        },
                        {
                            "value": "Comédie",
                            "label": "Comédie"
                        }
                    ],
                    actors: [{"name": "Bae Suzy"}, {"name": "Tim Allen"}, {"name": "Joan Cusack"}],
                    author: "Samy Issiakhem"
                },
            },

            // Model for film response
            FilmResponse: {
                type: "object", // Data type
                required: ["IdMovie", "title", "synopsis", "rating", "coverURL", "episodes", "actors", "author", "country", "duration", "movieState", "type", "genres"], // Required fields
                properties: {
                    IdMovie: { type: "string" },
                    title: { type: "string" },
                    synopsis: { type: "string" },
                    rating: { type: "string" },
                    coverURL: { type: "string" },
                    episodes: { type: "string" },
                    author: { $ref: '#/components/schemas/AuthorLink' },
                    actors: { type: "array", items: { $ref: '#/components/schemas/AuthorLink' } },
                    country: { type: "string" },
                    duration: { type: "string" },
                    movieState: { type: "string" },
                    type: { type: "string" },
                    genres: { type: "array", items: { type: "string" } }
                },
                example: {
                    IdMovie: "1",
                    title: "Crash Landing on You",
                    synopsis: "Crash Landing on You is an American film directed by Jon Favreau, released in 2016. It is an adaptation of Rudyard Kipling's novel, published in 1894. The film is produced by Walt Disney Pictures and distributed by Walt Disney Studios Motion Pictures. It was released in France on October 26, 2016.",
                    rating: "5",
                    coverURL: "https://m.media-amazon.com/images/M/MV5BMzRiZWUyN2YtNDI4YS00NTg2LTg0OTgtMGI2ZjU4ODQ4Yjk3XkEyXkFqcGdeQXVyNTI5NjIyMw@@._V1_.jpg",
                    episodes: "16",
                    author: {
                        idAuthor: "1",
                        name: "Rudyard Kipling",
                    },
                    actors: [
                        {
                            idAuthor: "1",
                            name: "Rudyard Kipling",
                        }
                    ],
                    country: "USA",
                    duration: "1h 45min",
                    movieState: "Available",
                    type: "Film",
                    genres: ["Action", "Comedy", "Drama"]
                },
            },

            // Model for film link
            FilmLink: {
                type: "object", // Data type
                required: ["IdMovie", "title", "coverURL", "type", "genres"], // Required fields
                properties: {
                    IdMovie: { type: "string" },
                    title: { type: "string" },
                    coverURL: { type: "string" },
                    type: { type: "string" },
                    genres: { type: "array", items: { type: "string" } }
                },
                example: {
                    IdMovie: "1",
                    title: "Crash Landing on You",
                    coverURL: "https://m.media-amazon.com/images/M/MV5BMzRiZWUyN2YtNDI4YS00NTg2LTg0OTgtMGI2ZjU4ODQ4Yjk3XkEyXkFqcGdeQXVyNTI5NjIyMw@@._V1_.jpg",
                    type: "Film",
                    genres: ["Action", "Comedy", "Drama"]
                },
            },

            // Model for album link
            AlbumLink: {
                type: "object", // Data type
                required: ["IdAlbum", "title", "coverURL"], // Required fields
                properties: {
                    IdAlbum: { type: "string" },
                    title: { type: "string" },
                    coverURL: { type: "string" },
                },
                example: {
                    IdAlbum: "1",
                    title: "Red (Taylor's Version)",
                    coverURL: "https://upload.wikimedia.org/wikipedia/en/0/0d/Taylor_Swift_-_Red_%28Taylor%27s_Version%29.png",
                },
            },

            // Model for album payload
            AlbumPayload: {
                type: "object", // Data type
                required: ["title", "releaseDate", "label", "tracks", "author", "coverURL", "albumState", "language"], // Required fields
                properties: {
                    title: { type: "string" },
                    releaseDate: { type: "string" },
                    label: { type: "string" },
                    author: { type: "string" },
                    tracks: {
                        type: "array",
                        items: {
                            type: "object",
                            $ref: '#/components/schemas/TrackPayload',
                        },
                    },
                    coverURL: { type: "string" },
                    albumState: { type: "string" },
                    language: { type: "string" },
                },
                example: {
                    title: "Red (Taylor's Version)",
                    releaseDate: "2021",
                    author: "Taylor Swift",
                    tracks: [
                        {
                            name: "State of Grace",
                            trackLength: "3:50",
                        }
                    ],
                    language: "English",
                    label: "Republic",
                    coverURL: "https://upload.wikimedia.org/wikipedia/en/4/47/Taylor_Swift_-_Red_%28Taylor%27s_Version%29.png",
                    albumState: "Disponible",
                },
            },

            // Model for track response
            TrackResponse: {
                type: "object", // Data type
                required: ["IdTrack", "name", "trackLength"], // Required fields
                properties: {
                    IdTrack: { type: "string" },
                    name: { type: "string" },
                    trackLength: { type: "string" },
                },
                example: {
                    IdTrack: "1",
                    name: "Come Back Be Here",
                    trackLength: "3:50",
                },
            },

            // Model for track payload
            TrackPayload: {
                type: "object", // Data type
                required: ["name", "trackLength"], // Required fields
                properties: {
                    name: { type: "string" },
                    trackLength: { type: "string" },
                },
                example: {
                    name: "Come Back Be Here",
                    trackLength: "3:50",
                },
            },

            // Model for album response
            AlbumResponse: {
                type: "object", // Data type
                required: ["IdAlbum", "title", "author", "releaseDate", "label", "tracks", "coverURL", "albumState"], // Required fields
                properties: {
                    IdAlbum: { type: "string" },
                    title: { type: "string" },
                    releaseDate: { type: "string" },
                    label: { type: "string" },
                    author: {
                        type: "object",
                        $ref: '#/components/schemas/AuthorLink',
                    },
                    tracks: {
                        type: "array",
                        items: {
                            type: "object",
                            $ref: '#/components/schemas/TrackResponse',
                        },
                    },
                    coverURL: { type: "string" },
                    albumState: { type: "string" }
                },
                example: {
                    IdAlbum: "1",
                    title: "Red (Taylor's Version)",
                    releaseDate: "2021",
                    label: "Republic",
                    author: {
                        idAuthor: "1",
                        name: "Taylor Swift",
                    },
                    tracks: [
                        {
                            IdTrack: "1",
                            name: "Come Back Be Here",
                            trackLength: "3:50",
                        }
                    ],
                    coverURL: "https://upload.wikimedia.org/wikipedia/en/4/47/Taylor_Swift_-_Red_%28Taylor%27s_Version%29.png",
                    albumState: "Disponible",
                    type: "Film",
                },
            },

            // Model for book link
            BookLink: {
                type: "object", // Data type
                required: ["IdBook", "title", "coverURL", "author", "type", "genres"], // Required fields
                properties: {
                    IdBook: { type: "string" },
                    title: { type: "string" },
                    coverURL: { type: "string" },
                    author: { type: "object" },
                    type: { type: "string" },
                    genres: { type: "array", items: { type: "string" } }
                },
                example: {
                    IdBook: "1",
                    title: "Le Livre de la Jungle",
                    coverURL: "https://books.google.ca/books/publisher/content?id=43g9DgAAQBAJ&hl=fr&pg=PP1&img=1&zoom=3&bul=1&sig=ACfU3U1-FoM1Y6Z6DTzED4CfFGMEbr-tuA&w=1280",
                    author: "Samy Issiakhem",
                    type: "Roman",
                    genres: ["Aventure", "Fantastique"],
                },
            },

            // Author link model
            AuthorLink: {
                type: "object",
                required: ["idAuthor", "authorName", "authorURL"],
                properties: {
                    idAuthor: { type: "string" },
                    authorName: { type: "string" },
                    authorURL: { type: "string" },
                },
                example: {
                    idAuthor: "1",
                    authorName: "Samy Issiakhem",
                    authorURL: "https://www.example.com/samy-issiakhem",
                },
            },

            // Author response model
            AuthorResponse: {
                type: "object", // Data type
                required: ["id", "authorName", "dateBirth", "occupation", "placeBirth", "books", "authorURL"],
                properties: {
                    id: { type: "string" },
                    authorName: { type: "string" },
                    dateBirth: { type: "string" },
                    occupation: { type: "string" },
                    placeBirth: { type: "string" },
                    books: {
                        type: "array",
                        items: {
                            type: "object",
                            $ref: '#/components/schemas/BookLink',
                        },
                    },
                    authorURL: { type: "string" },
                },
                example: {
                    id: "1",
                    authorName: "Samy Issiakhem",
                    dateBirth: "1990-01-01",
                    occupation: "Étudiant",
                    placeBirth: "Montréal",
                    books: [
                        {
                            IdBook: "1",
                            title: "Le livre de la jungle",
                            coverURL: "https://books.google.ca/books/publisher/content?id=43g9DgAAQBAJ&hl=fr&pg=PP1&img=1&zoom=3&bul=1&sig=ACfU3U1-FoM1Y6Z6DTzED4CfFGMEbr-tuA&w=1280",
                        }
                    ],
                    authorURL: "https://www.google.com",
                },
            },

            // Revised BookPayload model
            BookPayload: {
                type: "object", // data-type
                required: ["title", "ISBN", "plot", "releaseDate", "nbrPages", "author", "coverURL", "bookState", "type", "genres", "language"], // required fields
                properties: {
                    title: { type: "string" }, // data-type
                    ISBN: { type: "string" }, // data-type
                    plot: { type: "string" }, // data-type
                    releaseDate: { type: "string" }, // data-type
                    coverURL: { type: "string" }, // data-type
                    nbrPages: { type: "integer" }, // data-type
                    author: { type: "string" }, // data-type
                    bookState: { type: "string" }, // data-type
                    type: { type: "string" }, // data-type
                    language: { type: "string" }, // data-type
                    genres: {
                        type: "array",
                        items: { type: "string" },
                    },
                },
                example: {
                    title: "Le livre de la jungle",
                    ISBN: "978220729310",
                    plot: "Un livre de la jungle",
                    releaseDate: "2020-01-01",
                    nbrPages: 100,
                    coverURL: "https://books.google.ca/books/publisher/content?id=43g9DgAAQBAJ&hl=fr&pg=PP1&img=1&zoom=3&bul=1&sig=ACfU3U1-FoM1Y6Z6DTzED4CfFGMEbr-tuA&w=1280",
                    author: "Samy Issiakhem",
                    bookState: "Disponible",
                    type: "Roman",
                    language: "Français",
                    genres: [
                        {
                            "value": "Fantaisie",
                            "label": "Fantaisie"
                        },
                        {
                            "value": "Aventure",
                            "label": "Aventure"
                        }
                        ],
                },
            },


            // Comment payload schema
            CommentPayload: {
                type: "object", // Data type
                required: ["comment"], // Required fields
                properties: {
                    comment: { type: "string", },
                },
                example: {
                    comment: "A comment",
                },
            },

            // Comment response schema
            CommentResponse: {
                type: "object", // Data type
                required: ["comment"], // Required fields
                properties: {
                    comment: { type: "string", },
                },
                example: {
                    comment: "A comment",
                },
            },

            // Book response model
            BookResponse: {
                type: "object", // Data type
                required: ["IdBook", "author", "title", "ISBN", "plot", "releaseDate", "coverURL", "book_state", "type", "genres"], // Required fields
                properties: {
                    IdBook: { type: "string" }, // Book ID
                    author: { type: "object", $ref: '#/components/schemas/AuthorLink' }, // Author information
                    title: { type: "string" }, // Book title
                    ISBN: { type: "string" }, // International Standard Book Number
                    plot: { type: "string" }, // Plot or summary of the book
                    releaseDate: { type: "string" }, // Release date of the book
                    coverURL: { type: "string" }, // URL of the book cover image
                    bookState: { type: "string" }, // Book availability status
                    type: { type: "string" }, // Type or genre of the book
                    genres: { type: "array", items: { type: "string" } }, // Genres associated with the book
                },
                example: {
                    IdBook: "978220729310",
                    author: {
                        IdAuthor: "1",
                        authorName: "Samy Issiakhem",
                        authorURL: "https://www.example.com/samy-issiakhem",
                    },
                    title: "Le livre de la jungle",
                    ISBN: "978220729310",
                    plot: "Un livre de la jungle",
                    releaseDate: "2020-01-01",
                    coverURL: "https://books.google.ca/books/publisher/content?id=43g9DgAAQBAJ&hl=fr&pg=PP1&img=1&zoom=3&bul=1&sig=ACfU3U1-FoM1Y6Z6DTzED4CfFGMEbr-tuA&w=1280",
                    bookState: "Disponible",
                    type: "Roman",
                    genres: ["Aventure", "Fantastique"],
                },
            },

            // Success message model
            SuccessMessage: {
                type: "object",
                required: ["message", "success"],
                properties: {
                    message: { type: "string" }, // Descriptive success message
                    success: { type: "boolean" }, // Success indicator
                },
                example: {
                    message: "Un message de succès descriptif",
                    success: true,
                },
            },

            // Error message model
            ErrorMessage: {
                type: "object",
                required: ["message", "success"],
                properties: {
                    message: { type: "string" }, // Descriptive error message
                    success: { type: "boolean" }, // Success indicator (false for error)
                },
                example: {
                    message: "Un message d'erreur descriptif",
                    success: false,
                },
            },
        },
    },
};
