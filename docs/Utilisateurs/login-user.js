module.exports = {
  // method of operation
  post: {
    tags: ["Utilisateurs"], // operation's tag.
    summary: "Get an authentication token", // operation's description.
    operationId: "loginUser", // unique operation id.
    parameters: [], // expected params.
    requestBody: {
      required: true, // Mandatory param
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/TokenCreationPayload", // user data model
          },
        },
      },
    },
    // expected responses
    responses: {
      // response code
      200: {
        description: "The token", // response description.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/TokenCreationResponse", // user data model
            },
          },
        },
      },
      // response code
      403: {
        description: "Response when the provided password is incorrect", // response description.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // user data model
            },
          },
        },
      },
      // response code
      404: {
        description: "Response when the user is not found", // response description.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // error data model
            },
          },
        },
      },
      // response code
      400: {
        description: "Response when the parameter is invalid or missing data", // response description.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // error data model
            },
          },
        },
      },
      // response code
      500: {
        description: "Response when the server encounters a situation it does not know how to handle", // response description.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ErrorMessage", // error data model
            },
          },
        },
      },
    },
  },
};
