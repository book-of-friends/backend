module.exports = {
    // operation's method
    get: {
      tags: ["Albums"], // operation's tag.
      summary: "liste des albums dans la base de données", // operation's desc.
      operationId: "getAllAlbums", // unique operation email
      // expected responses
      responses: {
        // response code
        200: {
          description: "les données des albums sont retournées", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/AlbumLink", // user data model
              },
            },
          },
        },
        // response code
        404: {
          description: "réponse si aucun film n'est trouvé", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
        // response code
        500: {
          description: "réponse si le serveur a rencontré une situation qu'il ne sait pas gérer", // response desc.
          content: {
            // content-type
            "application/json": {
              schema: {
                $ref: "#/components/schemas/ErrorMessage", // error data model
              },
            },
          },
        },
      },
    },
  };