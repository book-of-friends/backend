const loginUser = require('./utilisateurs/login-user');
const registerUser = require('./utilisateurs/register-user');
const get_user = require('./utilisateurs/get-user');
const updatePassword = require('./Utilisateurs/update-password');
const resetPassword = require('./utilisateurs/reset-password');
const delete_user = require('./utilisateurs/delete-user');
const updateUser = require('./utilisateurs/update-user');
const updateEmail = require('./utilisateurs/update-email');
const get_all_books = require('./Books/all-books');
const get_book = require('./Books/get-book');
const add_book = require('./Books/add-book');
const delete_book = require('./Books/delete-book');
const update_book = require('./Books/update-book');
const get_all_movies = require('./Movies/all-movies');
const get_movie = require('./Movies/get-movie');
const add_movie = require('./Movies/add-movie');
const delete_movie = require('./Movies/delete-movie');
const update_movie = require('./Movies/update-movie');
const get_all_albums = require('./Albums/all-albums');
const add_album = require('./Albums/add-album');
const delete_album = require('./Albums/delete-album');
const update_album = require('./Albums/update-album');
const get_album = require('./Albums/get-album');
const get_mangas = require('./Books/get-mangas');
const get_romans = require('./Books/get-romans');
const get_All = require('./Books/get-all');
const get_similar = require('./Albums/get-similarities');
const makeReserve = require('./Reservations/make-reservation');
const annuleReservation = require('./Reservations/delete-reservation');
const getReserve = require('./Reservations/get-reservation');
const getReserves = require('./Reservations/get-all-reservations');
const getComment = require('./Comments/getComments');
const postComment = require('./Comments/postComments');
const imageUpload = require('./Similarities/imageUpload');


module.exports = {
    paths:{
        '/auth/token':
            {
                ...loginUser
            },
        '/auth/register':
            {
                ...registerUser
            },
        '/user':
            {
                ...get_user,
                ...delete_user,
                ...updateUser
            },
        '/user/update-password':
            {
                ...updatePassword
            },
        '/user/password-oublie':
            {
                ...resetPassword
            },
        '/user/update-email':
            {
                ...updateEmail
            },
        '/similar':
            {
                ...get_similar,
            },
        '/products':
            {
                ...get_All,
            },
        '/books':
            {
                ...get_all_books,
            },
        '/mangas':
            {
                ...get_mangas,
            },
        '/romans':
            {
                ...get_romans,
            },
        '/book':
            {
                ...get_book,
                ...delete_book,
                ...update_book,
                ...add_book,
            },
        '/reservations':
            {
                ...getReserves,
            },
        '/upload-cover':
            {
                ...imageUpload,
            },
        '/movies':
            {
                ...get_all_movies,
            },
        '/movie':
            {
                ...add_movie,
                ...get_movie,
                ...delete_movie,
                ...update_movie
            },
        '/reservation':
            {
                ...makeReserve,
                ...annuleReservation,
                ...getReserve
            },
        '/comment':
            {
                ...getComment,
                ...postComment
            },
        '/albums':
            {
                ...get_all_albums,
            },
        '/album':
            {
                ...add_album,
                ...get_album,
                ...delete_album,
                ...update_album
            },
    }
}