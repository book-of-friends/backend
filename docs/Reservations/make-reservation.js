module.exports = {
    // method of operation
    post: {
        tags: ["Reservations"], // operation's tag.
        security: [
            {
                bearerAuth: []
            }
        ],
        summary: "Route to make a reservation", // operation's desc.
        operationId: "addReserve", // unique operation id.
        parameters: [
            {
                $ref: '#/components/parameters/Id' // data model of the param
            },
            {
                $ref: '#/components/parameters/IdNum' // data model of the param
            },
            {
                $ref: '#/components/parameters/Type' // data model of the param
            }
        ], // expected params.
        requestBody: {},
        // expected responses
        responses: {
            // response code
            201: {
                description: "Response when the document is successfully reserved", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/SuccessMessage", // User model
                        },
                    },
                },
            },
            // response code
            401: {
                description: "Response if the user is not logged in", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // user data model
                        },
                    },
                }
            },
            // response code
            404: {
                description: "Response if the book or user is not found", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // user data model
                        },
                    },
                },
            },
            // response code
            400: {
                description: "Response if the parameter is invalid or lacks data for addition", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
            // response code
            500: {
                description: "Response if the server encounters a situation it cannot handle", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
        },
    },
};
