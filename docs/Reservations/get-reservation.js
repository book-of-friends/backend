module.exports = {
    // operation's method
    get: {
        tags: ["Reservations"], // operation's tag.
        security: [
            {
                bearerAuth: []
            }
        ],
        summary: "List of reservations for a user", // operation's desc.
        operationId: "getAllReservation", // unique operation email
        // expected responses
        responses: {
            // response code
            200: {
                description: "Reservations of the logged-in user are returned", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/BookLink", // user data model
                        },
                    },
                },
            },
            // response code
            404: {
                description: "Response if no reservation is found", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
            // response code
            401: {
                description: "Response if the user is not logged in", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // user data model
                        },
                    },
                },
            },
            // response code
            500: {
                description: "Response if the server encounters a situation it cannot handle", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
        },
    },
};
