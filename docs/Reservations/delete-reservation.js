module.exports = {
    // method of operation
    delete: {
        tags: ["Reservations"], // operation's tag.
        security: [
            {
                bearerAuth: []
            }
        ],
        summary: "Route to cancel a reservation", // operation's desc.
        operationId: "deleteReserve", // unique operation id.
        parameters: [
            {
                $ref: '#/components/parameters/Id' // data model of the param
            }
        ], // expected params.
        requestBody: {},
        // expected responses
        responses: {
            // response code
            201: {
                description: "Response when the reservation is canceled", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/SuccessMessage", // User model
                        },
                    },
                },
            },
            // response code
            401: {
                description: "Response if the user is not logged in", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // user data model
                        },
                    },
                }
            },
            // response code
            404: {
                description: "Response if no reservation is found", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // user data model
                        },
                    },
                },
            },
            // response code
            400: {
                description: "Response if the parameter is invalid or missing data for addition", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
            // response code
            403: {
                description: "Response if the user who made the reservation is not the same", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
            // response code
            500: {
                description: "Response if the server encountered a situation it cannot handle", // response desc.
                content: {
                    // content-type
                    "application/json": {
                        schema: {
                            $ref: "#/components/schemas/ErrorMessage", // error data model
                        },
                    },
                },
            },
        },
    },
};
