const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const bookSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    ISBN: String,
    title: {
        type: String,
        required: true
    },
    plot: String,
    releaseDate: String,
    author: String,
    coverURL: String,
    nbrPages: String,
    genres: [
        {
            value: String,
            label: String
        }
    ],
    bookState: String,
    language: String,
    type: String,
}, { versionKey: false  });

const Book = model('Book', bookSchema);

module.exports = Book;
