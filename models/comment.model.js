const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const commentSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    comment: {
        type: String,
        required: true
    },
}, { versionKey: false  });


const Comment = model('Comment', commentSchema);
module.exports = Comment;
