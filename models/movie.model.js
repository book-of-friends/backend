const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const actorSchema = new Schema({
    name: String
}, { _id: false });


const movieSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    title: {
        type: String,
        required: true
    },
    synopsis: String,
    rating: String,
    coverURL: String,
    releaseDate: String,
    seasons: String,
    author: String,
    genres: [
        {
            value: String,
            label: String
        }
    ],
    country: String,
    duration: String,
    language: String,
    movieState: String,
    type: String,
    actors: [actorSchema]
}, { versionKey: false  });


const Movie = model('Movie', movieSchema);
module.exports = Movie;
