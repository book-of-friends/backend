const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const userSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    numDossier: String,
    firstname: String,
    lastname: String,
    phone: String,
    email: String,
    password: String,
    address: String,
    author: String,
    country: String,
    dateOfBirth: String,
    codePostal: String,
    city: String,
    province: String,
    admin: Boolean,
}, { versionKey: false  });

const User = model('User', userSchema);
module.exports = User;
