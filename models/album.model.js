const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const trackSchema = new Schema({
    name: String
}, { _id: false });

const albumSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    title: {
        type: String,
        required: true
    },
    releaseDate: String,
    author: String,
    label: String,
    coverURL: String,
    albumState: String,
    tracks: [trackSchema],
    language: String,
}, { versionKey: false  });


const Album = model('Album', albumSchema);
module.exports = Album;
