const {Schema, model, connect} = require("mongoose");
const config = require("../utils/config");

connect(config.MONGODB_URI)

const reservationSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    articleId: String,
    userId: String,
    state: String,
    dateBorrow: String,
    dateReturn: String,
    type: String,
    articleNumber: String,
}, { versionKey: false  });


const Reservation = model('Reservation', reservationSchema);
module.exports = Reservation;
