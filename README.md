# Book Of Friends - API (Backend) : EN COURS



## Description

Dans le cadre du cours de projet intégrateur, J'ai développé une application web destiné pour une petite librairie fictif qui vend des livres, bandes dessinées/mangas, films et albums. L’application permettra au client (utilisateur) d’avoir une liste d’œuvre et de réserver un article et voir si celui-ci est disponible en magasin ou pas. Les utilisateurs doivent être connectés pour réserver une œuvre ou s’inscrire s’ils n’ont pas de compte. Elle permettra aussi à l'administrateur de consulter, modifier, ajouter et supprimer un document. 

Pour développer le coté serveur de l'application j'ai utilisé MongoDB comme base de données pour les documents ainsi que les réservations. J'ai utilisé Firebase pour la gestion et l'authentification des utilisateurs avec les outils authentication et Realtime Database. Pour le stockage des images et des ficheirs J'ai utilisé l'outil Storage fournit par Firebase. Le tout est développé en Javascript comme language de programmation.NodeMailer a été aussi utilisé pour l'envoie des courriels lors des réservations.

l'API fournit plusieurs routes pour la gestion des utlisateurs, des documents ainsi que les reservations.

Pour avoir une application logique et réaliste j'ai pris comme exemple l'application web de la grande bibliotheque de montréal https://cap.banq.qc.ca/accueil

## Installation

pour pouvoir utiliser l'API veuillez suivre les instructions:

### 1- Faire un clone du projet localement 
lancez la commande git clone https://gitlab.com/book-of-friends/backend.git dans cotre terminal

### 2- Placez vous dans le bon répertoire soit Backend

### 3- veuillez ensuite installer les dépendances nécessaires
npm install ou npm i

### 4- Lancez la commande suivante dans le terminal pour lancer le serveur
npm start / node index.js / nodemon index.js


### 5- Pour accéder à l'API, appuyer sur le lien
http://localhost:3000


## Support
En cas de problemess ou de difficultées contactez samylichine.iss@gmail.com

## contributions
Ce projet est développé par: Samy Issiakhem

